#################################
Installation on Microsoft Windows
#################################

.. toctree::
   :hidden:

Installation with the installer
===============================

The recommended way to install Spview on Microsoft Windows is to use the 
provided installer which will automatically install the application. However,
when you start the installation, Windows Defender blocks the installation to 
"protect" your computer with the message ``Windows protected your PC``.

.. figure:: ../_images/installation/spview-win1.png
   :alt: installer_start
   :class: with-shadow
    
To solve this problem, simply trust the Spview installer:

* Click on the :guilabel:`More info` link below the Windows Defender message.
* A new button is available at the bottom of your window. Click on :guilabel:`Run anyway`
  to launch the installation.
   
.. figure:: ../_images/installation/spview-win2.png
   :alt: installer_start
   :class: with-shadow

Then the application will be installed in the ``C:\Program Files\Spview`` folder.

.. figure:: ../_images/installation/spview-win3.png
   :alt: installer_end
   :class: with-shadow

