#########################
Installation on GNU/Linux
#########################

.. toctree::
   :hidden:

Installation of debian package
==============================

For GNU/Linux systems, we've decided to provide a ``deb`` binary which runs on 
Debian-Like x86_64 systems (such as Debian, Ubuntu, Linux Mint, ...). To install 
the binary, simply run the following command, in root:

.. code-block:: bash
      
      dpkg -i spview_x.y.z_all.deb

Replace x, y and z with the version numbers. Spview will then be installed on 
your system, and you can launch it using the dedicated icon.
