Toolbar
=======

Spview has two toolbars. The first lets you move around a spectrum along the x 
axis. This can be done step by step, page by page, or directly at the beginning 
or end of the spectrum. The second moves along the y-axis. This is useful when 
applying an offset between experiment and prediction, or when zooming.

.. figure:: ../_images/GUI/toolbar_bottom.png
   :alt: Toolbar at the bottom
   :class: with-shadow
   
   Main toolbar of Spview. It is used to move through the spectrum along the x 
   axis.
   
.. figure:: ../_images/GUI/toolbar_left.png
   :alt: Toolbar at the bottom
   :class: with-shadow
   
   Second toolbar of Spview. It is used to move through the spectrum along the y 
   axis.
