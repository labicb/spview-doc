Menubar
=======

All the spview tools are stored in the menubar. It contains everything needed
for the spectra analysis.
The menubar is composed of:

* :ref:`File <GUI/menubar:File>`
* :ref:`Data <GUI/menubar:Data>`
* :ref:`DataManagement <GUI/menubar:DataManagement>`
* :ref:`Assignment <GUI/menubar:Assignment>`
* :ref:`Plot <GUI/menubar:Plot>`
* :ref:`Help <GUI/menubar:Help>`

File
~~~~
The :guilabel:`File` menu is dedicated to Spview project management.
Spview creates project files with the extension :file:`.spv`. These files, of 
:file:`xml` type, contain the absolute paths of the source files used in the 
project and are described in details :ref:`here <file-format/Spv:SPV File Format>`.

.. note::
   Distributing :file:`.spv` files from one computer to another will not allow 
   you to recover the project, because as mentioned above, file paths in the 
   :file:`xml` file are absolute.

* The :guilabel:`Open Project...` menu, accessible via the :kbd:`Ctrl` + 
  :kbd:`O` shortcut, opens :file:`.spv` files.
* The menu :guilabel:`Open recent` is a handy menu for opening the latest 
  projects in use. The :file:`.spv` files and sources must still be available 
  and in the same place as when last used.
* To close the loaded project, hit :guilabel:`Close Project` or use the shortcut
  :kbd:`Ctrl` + :kbd:`W`.
* The :guilabel:`Save Project As...` item (:kbd:`Ctrl` + :kbd:`Shift` + :kbd:`S`) 
  saves the project the first time, under a name to be defined. It can also be 
  used to save the current project under another name.
* The second save item, :guilabel:`Save` (:kbd:`Ctrl` + :kbd:`S`), saves the 
  currently loaded project in the same location as previously defined with 
  :guilabel:`Save Project As...`.
* Finally, the last element, :guilabel:`Exit`, with the shortcut (:kbd:`Ctrl` + 
  :kbd:`Q`) is used to quit the application.
  

Data
~~~~
This is the menu used to load most of the data used in the software.

* The :guilabel:`Load spectrum file` menu is used to load experimental or simulated data. 
  These can take two forms. Either a 2-column text file containing positions 
  and intensities, or an OPUS binary file from Bruker. The file extension is 
  irrelevant and is not taken into account. Spview first tries to define whether
  the loaded file is of type :ref:`here <file-format/OPUS:OPUS File Format>`.
  If not, it assumes that the file is of 
  :ref:`here <file-format/2-columns:2-columns File Format>` type. Files are
  displayed in the central window. 
* The :guilabel:`Load stick file` menu handles the same files as the previous 
  menu. However, the loaded files are displayed as stick spectra in the central
  window.
* The :guilabel:`Load (as stick) peak/experiment file` menu loads files in the format 
  created by the peak detector. If the file format does not meet the 
  requirements, an error is triggered and the file is not loaded. This format,
  explained in details :ref:`here <file-format/Asg:Assignment File Format>`, is 
  the format read by the fit jobs created by XTDS. The loaded files are 
  displayed as stick spectra in the central window.
* The :guilabel:`Load (as stick) prediction file` menu is used to load 
  files which may be in two different formats, 
  :ref:`TDS <file-format/TDS:TDS File Format>` and 
  :ref:`HITRAN (2000 or 2004)<file-format/HITRAN:HITRAN File Format>`. As a 
  result, :guilabel:`Load prediction file` has two sub-menus, allowing us to 
  load one or the other. Again, the loaded files are displayed as stick spectra
  in the main central window.


DataManagement
~~~~~~~~~~~~~~
This is the menu for managing data loaded into Spview. As spectra are loaded, 
and their data plot on screen, the file name is added to this menu.

.. figure:: ../_images/GUI/data_management.png
   :alt: menu
   :class: with-shadow
   
It is then possible to :guilabel:`Show`/:guilabel:`Hide` the plots. 
:guilabel:`Reload` data if it has changed, and shift curves in X or Y axes with 
:guilabel:`X Shift`/:guilabel:`X` unshift and :guilabel:`Y Shift`/
:guilabel:`Y unshift`. Finally, you can reverse the y-axis with 
:guilabel:`Y reverse`.

The :guilabel:`Restore all in basic state` button, as its name suggests, resets 
all the states of each plot.


Assignment
~~~~~~~~~~
Spview’s key features reside in the :guilabel:`Assignment` menu. With this menu, 
it is possible to load an experimental peaklist and a calculated prediction which
are displayed as small ticks for each line in the top panel.

  .. figure:: ../_images/GUI/Spview_assignment.png
     :alt: Assignment
     :class: with-shadow
     
Figure above shows an example that we detail a bit below. The experimental 
peaklist is displayed in the upper half of this top panel. Clicking on each tick
allows to select it. The lower half of the panel displays the predicted lines. 
Dragging a region in this part with the mouse (grayed area in the figure) 
deploys a popup window showing the different calculated lines lying in this 
region, with their quantum numbers. It is then possible to assign one or more of
them (in position, intensity or both) to a previously selected experimental 
line. A right-click in the upper panel also allows different manipulations, like
unassigning a line, changing its status (position or intensity assignment, etc).
It is also possible to change the :ref:`character chain <file-format/Asg:Assignment File Format>` 
that is happened to each assignment and used by the fit job created by XTDS to 
extract the lines to be included in the fit.
  
* The first tool available in the menu is entitled :guilabel:`Create a peak file 
  from a spectrum file`. It offers the possibility of detecting peaks on spectra 
  previously loaded or not. In fact, the menu offers the option of directly 
  opening the loaded files, or a :guilabel:`Unknown default spectrum file name`
  option. The algorithm used for peak detection is called "digital filter" and is 
  explained below.
 
  .. figure:: ../_images/GUI/peak_detection.png
     :alt: Peak detection
     :class: with-shadow
   
  There are several options in the opened dialog. 

    * The first is the spectrum path where the peak will be detected. It is 
      automatically filled in if the user has chosen a spectrum already displayed. 
      Otherwise, click on :guilabel:`Spectrum file...` to browse the files and choose 
      the right one.
    * The :guilabel:`Filter Width` is the width of the single-cycle square wave as
      explained in the algorithm below. By default the value is of 7 and is good
      enough in many cases.
    * The :guilabel:`Height Threshold` entry defines the peak detection threshold. 
      Enter a value higher than the noise value. It is necessary to hit 
      :guilabel:`Down` if the peak is upside down.
    * :guilabel:`X Uncertainty` and :guilabel:`Y Uncertainty` are experimental 
      uncertainties. Uncertainty Y is expressed as a percentage.
  
    The :guilabel:`Find Peaks` button initiates peak detection, and a dialog box 
    opens to indicate the number of peaks detected. By clicking on :guilabel:`OK`, 
    you are encouraged to save a text file containing all the peaks. This file will 
    be the assignment file and will be used throughout the work. You can of course
    click on :guilabel:`Cancel` to try another detection by changing some parameters.
    
* The :guilabel:`Load peak/experiment file` menu loads a file in the format 
  created by the peak detector. If the file format does not meet the 
  requirements, an error is triggered and the file is not loaded. This format,
  explained in details :ref:`here <file-format/Asg:Assignment File Format>`, is the
  format read by the fit jobs created by XTDS. The file name is displayed in the
  bottom right-hand corner of the spectrum viewer frame. Moreover, when a line 
  is assigned, SPVIEW automatically fills this file, so there's no need to save 
  the assignment each time it changes.

* The prediction files used in Spview can be of two different formats, 
  :ref:`TDS <file-format/TDS:TDS File Format>` and 
  :ref:`HITRAN (2000 or 2004)<file-format/HITRAN:HITRAN File Format>`. As a 
  result, :guilabel:`Load prediction file` has two sub-menus, allowing us to 
  load either one or the other.

* The following 2 menus, :guilabel:`Associate experiment to data` and
  :guilabel:`Associate prediction to data` allow you to associate experimental 
  and simulated spectra with peak readings and predictions. The association will 
  be effective when identical colors are assigned. The association can be 
  removed by choosing null in the submenu.

* When the assignment becomes more complex, it may be useful to save it in 
  another file. The :guilabel:`Save assignment file as...` menu is dedicated to 
  this. Once saved, load the new file using the :guilabel:`Load peak/experiment 
  file` menu.

The peak finder: The Digital Filter
-----------------------------------

The method, called the "digital filter", starts with an algorithm that 
convolutes the data with a single-cycle square wave of width :math:`W` and 
amplitude 1. The data is stored as an array of integers; each data point is 
referenced by its offset from the beginning of the array (array index). The 
convolution is computed entirely by addition and subtraction, for example:

The value of the convolution for array index :math:`N` is computed by summing 
together all of the input data points from index :math:`N` to :math:`N+W/2`, 
then subtracting all of the data points from index :math:`N-1` to 
:math:`N-1-W/2`. An extremum (minimum or maximum) is located where the 
convolution changes sign. The direction of the sign change indicates whether the 
extremum is a minimum or a maximum. At locations where the convolution changes 
sign, an interpolation is performed to locate the extremum to within a fraction 
of one data point spacing. The meaningful accuracy of this interpolation is a 
function of noise in the data and the shape of the data in the region of 
extremum. Figure below illustrates the operation of a digital filter.

.. figure:: ../_images/GUI/digital_filter.png
   :alt: Digital Filter
   :class: with-shadow
   
   Principle of Operation of a Digital Filter

Plot
~~~~

During line assignments, it may be useful to check obs.-calc. to see if there 
are any outliers. To this end, this menu has two main entries: 
:guilabel:`X Obs.-Calc.` and :guilabel:`Y Obs.-Calc.` respectively used for 
position and intensity fits.

.. figure:: ../_images/GUI/plot_x_obs_cal.png
   :alt: plot
   :class: with-shadow
   
A single-click on a data point, as shown in the figure below, opens a new window 
providing the information needed to identify the line. In addition, a 
double-click the on the point, and the main window zooms in and moves to the 
relevant region of the spectrum, placing the possible outlier in the center.

.. figure:: ../_images/GUI/pred_win.png
   :alt: prediction window
   :class: with-shadow
   
Help
~~~~

This menu opens the :guilabel:`About` window, detailing the software and copyrights. 
Click :ref:`here <About:About Spview>` to see the text of this dialog box.
