About Spview
============

General Disclaimer
------------------

SPVIEW (SPectrum-VIEWer) allows to assign experimental high-resolution
molecular spectra. The input elements are:

-  Experimental and calculated data (Data menu): either XY files
   (experimental trace or stick spectrum) or line lists in various
   formats. OPUS files from Bruker can also be read.
-  Experimental peaks and predicted lines (Assignment menu).

The data are plotted in the center of the window (data area) and the
peaks and predicted lines are displayed as ticks in the upper part
(pred/exp area). The peaks and/or predicted lines ticks can be
associated to one of the data files so that they follow its drawing
modifications (X shifts). The association is shown by a common color.

Each assignment is either of frequency or intensity type. Each type is
specified by a mark which should be either a space (not assigned with
this type), a '+' (to be fitted with this type) or a '-' (to be ignored
in the fit with this type). Experimental ticks are topped by global
frequency and intensity marks. Each global mark is either a space (if no
assignment is of this type), a '+' or a '-' (if all assignments of this
type have this mark) or a '?' (if assignments of this type do not have
all the same mark).

A so-called EXASG string is associated to each assignment. It is used by
the fit job to select the lines to be taken into account. The initial
default value of this string for new assignments is 'SPVIEW_EXASG'. This
default value can be modified.

The pred/exp area ticks can be selected with the mouse and in this way
predicted lines can be assigned to each experimental peak (see Popup
menus below). Once this is done, one can save the assignment file
(Assignment menu).

Popup menus:
------------

-  In data area:

   -  Right-click (Command-click, ...) allows to rescale the axes and to
      set/reset a vertical bar which helps to visualize frequency
      coincidences.

-  In pred/exp area:

   -  Left-click in exp area (upper part of the pred/exp area) allows to
      select one experimental peak in a popup list.
   -  Right-click (Command-click, ...) allows to modify the attributes
      of the selected experimental peak (assignments, ...).
   -  Left-click or left button drag allows to select one or more
      prediction lines in a popup list.
   -  The current selected exp is marked by a red oval.
   -  The current selected (in the 'Predictions selection' popup list)
      pred are marked by green ovals.
   -  The current mouse pointed (in the 'Predictions selection' popup
      list) pred is marked by a yellow square).
   -  The current assigned (to any exp) pred are marked by an orange
      disc.
   -  The current assigned (to the selected exp) pred are marked by a
      red disc.

Contact
-------

-  `Vincent.Boudon@u-bourgogne.fr
   (Theory) <mailto:vincent.boudon@u-bourgogne.fr>`__
-  `Cyril.Richard@u-bourgogne.fr (Theory and Java
   programming) <mailto:cyril.richard@u-bourgogne.fr>`__

Website
-------

-  https://icb.u-bourgogne.fr/spview/
