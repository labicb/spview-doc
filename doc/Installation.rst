############
Installation
############

Each version of Spview are distributed for the 3 most common platforms (Windows,
MacOS, GNU / Linux) and can be downloaded on the 
`Spview website <https://icb.u-bourgogne.fr/spview/>`_. But of course, as Spview 
is a free software distributed under the GPLv3 license, you can build the 
application from the sources. The code is written in Java, making it easy to 
compile.

.. toctree::
   :hidden:

   installation/windows
   installation/linux
   installation/macos

