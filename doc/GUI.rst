Graphical User Interface
========================

Spview's graphical interface is very simple and consists of:

* :ref:`A menu bar <GUI/menubar:Menubar>`.
* :ref:`A main window <GUI/main-window:Main Window>` displaying spectra.
* :ref:`Two toolbars <GUI/toolbar:Toolbar>` for moving horizontally and vertically in the spectra.
* :ref:`Different popup menus <GUI/popupmenu:Popup menus>`.

The following subsections take you through the main interface window and 
useful menus.

.. toctree::
   :hidden:
   
   GUI/menubar
   GUI/main-window
   GUI/toolbar
   GUI/popupmenu
