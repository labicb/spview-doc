2-columns File Format
=====================

The 2-column file format used in Spview is designed to store positional and 
intensity data of lines. This is the simpliest way to represent a spectrum where
each row in the file corresponds to a line. The first column contains the 
position of the line, generally in :math:`\text{cm}^{-1}`, and the second column 
stores the corresponding intensity value. The two colums are separated by a 
**coma** or a **space**.

Here's an example of such a file:

.. code-block:: text

   419.99989,0.01047
   420.00001,0.00760
   420.00012,0.00376
   420.00024,-0.00003
   420.00036,-0.00285
   420.00048,-0.00407
   420.00060,-0.00359
   420.00071,-0.00178
   420.00083,0.00063
   420.00095,0.00284
   420.00107,0.00418
   420.00118,0.00432
   420.00130,0.00331
   420.00142,0.00151

