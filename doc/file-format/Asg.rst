Assignment File Format
======================

XTDS packages provides the possibility to fit hamiltonian parameters using 
experimental data. This necessitates an assignment file containing lines of the 
following form (the format must be respected):

.. code-block::

   .....................................................................................
     7466  870.614507 + 0.2150E+00    0.001000 10.0    13 F2   3  12 F1  15 SPVIEW_EXASG
     7467  870.620029 + 0.1490E+00    0.001000 10.0    13 E    2  12 E   11 SPVIEW_EXASG
     7468  870.625041 + 0.2430E+00    0.001000 10.0    13 F1   4  12 F2  16 SPVIEW_EXASG
     7469  870.644365 + 0.5500E-01    0.001000 10.0     9 A1   1  10 A2   2 SPVIEW_EXASG
     7470  870.669249   0.1660E+00    0.001000 10.0
     7471  870.674772   0.1150E+00    0.001000 10.0
     7472  870.679803   0.1930E+00    0.001000 10.0
     7473  870.784119   0.3700E-01    0.001000 10.0
     7474  870.985433   0.3100E-01    0.001000 10.0
     7475  871.024952   0.2200E-01    0.001000 10.0
     7476  871.038518   0.3900E-01    0.001000 10.0
     7477  871.065933   0.4200E-01    0.001000 10.0
     7478  871.246246   0.2300E-01    0.001000 10.0
     7479  871.341410   0.2300E-01    0.001000 10.0
     7480  871.426834   0.6400E-01    0.001000 10.0
     7481  871.485324   0.1240E+00    0.001000 10.0
     7482  871.716760   0.3600E-01    0.001000 10.0
     7483  871.891580 + 0.5300E-01    0.001000 10.0    24 F1   2  25 F2   5 SPVIEW_EXASG
     7483  871.891580 + 0.5300E-01    0.001000 10.0    24 F2   1  25 F1   6 SPVIEW_EXASG
     7484  871.924283   0.2400E-01    0.001000 10.0
     7485  871.955661 + 0.9300E-01    0.001000 10.0    23 F1   1  24 F2   6 SPVIEW_EXASG
     7485  871.955661 + 0.9300E-01    0.001000 10.0    23 F2   1  24 F1   6 SPVIEW_EXASG
   .....................................................................................
   
Each line has the following structure:

+---------------+----------------------------------------------------------------------------------+
| Columns 1+6   | Running index on experimental lines.                                             |
+---------------+----------------------------------------------------------------------------------+
| Columns 9+18  | Experimental line wavenumber.                                                    |
+---------------+----------------------------------------------------------------------------------+
| Column 20     | The plus sign indicates that the line has been assigned and fitted in position.  |
|               | If any other character is placed here, this indicates that the line has been     |
|               | assigned but will not be fitted and will thus not be taken into account by the   |
|               | programs.                                                                        |
+---------------+----------------------------------------------------------------------------------+
| Columns 24+31 | Experimental line intensity.                                                     |
+---------------+----------------------------------------------------------------------------------+
| Column 33     | The plus sign indicates that the line has been assigned and fitted in intensity. |
|               | If any other character is placed here, this indicates that the line has been     |
|               | assigned but will not be fitted and will thus not be taken into account by the   |
|               | programs.                                                                        |
+---------------+----------------------------------------------------------------------------------+
| Columns 36+43 | Experimental precision on line position.                                         |
+---------------+----------------------------------------------------------------------------------+
| Columns 45+48 | Experimental precision on line intencity (percentage).                           |
+---------------+----------------------------------------------------------------------------------+
| Columns 53+70 | Assignment in the form :math:`J''C''n''` (lower level) :math:`JCn`               |
|               | (upper level). Each experimental line can be assigned to several theoretical     |
|               | transitions if necessary.                                                        |
+---------------+----------------------------------------------------------------------------------+
| Columns 72+83 | Character chain used to extract desired assignment lines.                        |
+---------------+----------------------------------------------------------------------------------+




