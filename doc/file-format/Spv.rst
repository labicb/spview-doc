SPV File Format
===============

Availability and Use of SPV
---------------------------

Spview uses its own file format for easy project management. Files are of type 
:file:`xml` and have the extension :file:`.spv`. They are created by Spview and 
can be used to quickly open several work files such as predictions, experimental 
spectra, etc...

.. |spview-icon| image:: ../_images/Spv/text-x-spv.svg

* They are identified by the spview icon |spview-icon|.
* They contain the absolute path of all files, and therefore need the source 
  files to open. Moreover, moving the source files renders the :file:`.spv` 
  files invalid. 
* These also contain the zoom, offset values and many information used to view 
  the spectra.
* Double-clicking on a file opens the associated project in spview.

Here's an example of such a file, once indented:

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <Project>
      <Version />
      2.0.2
      <Zoom>
         <xmin>700.0</xmin>
         <xmax>1100.000238</xmax>
         <ymin>-0.6756958044164048</ymin>
         <ymax>1.209251829652997</ymax>
      </Zoom>
      <File AttributeOfFile="dataread">
         <path Type="spectrum">/home/user/path/Spectre_GeH4_PL5.asc</path>
         <yReverse>false</yReverse>
         <hide>false</hide>
         <xShiftState>0.0</xShiftState>
         <yShiftState>0.0</yShiftState>
      </File>
      <File AttributeOfFile="dataread">
         <path Type="spectrum">/home/user/path/simul.xy</path>
         <yReverse>false</yReverse>
         <hide>false</hide>
         <xShiftState>0.0</xShiftState>
         <yShiftState>-0.4424834700315457</yShiftState>
      </File>
      <File AttributeOfFile="predread">
         <path Type="TDS">/home/user/path/spectr.t</path>
         <indexColorPred>1</indexColorPred>
      </File>
      <File AttributeOfFile="expread">
         <path Type="peakExp">/home/user/path/assing72_06.asg</path>
         <indexColorExp>0</indexColorExp>
      </File>
   </Project>
   
Overview of SPV Features
------------------------
