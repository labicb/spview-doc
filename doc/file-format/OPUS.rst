OPUS File Format
================

The OPUS file format, developed by Bruker, is a versatile and widely used file 
format in the field of spectroscopy and materials analysis. Designed to store 
and exchange spectroscopic data, OPUS provides a comprehensive framework for 
organizing and preserving various types of spectroscopic information. This 
format seamlessly integrates spectral data, experimental parameters, and 
metadata into a single file, ensuring data integrity and ease of access. 
However, the format is proprietary and closed, so there's no guarantee that 
Spview can open all file types. OPUS files can contain diverse spectroscopic 
techniques such as infrared (IR), Raman, nuclear magnetic resonance (NMR), and 
more. They can be loaded into Spview with the menu :menuselection:`Data--> Load spectrum file`.
