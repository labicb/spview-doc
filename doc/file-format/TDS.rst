TDS File Format
===============

TDS files are usually named :file:`spectr.t` and are outputed by XTDS 
packages. An example is given below. The beginning of this file recalls the 
parameter values, the vibrational states involved and the calculation 
conditions. Then, the list of calculated transitions is given. Each line is 
identified by the lower :math:`(J'', n'', C'')` and upper :math:`(J, n, C)` quantum
numbers. In each case, the :math:`(J, n, C)` sets are followed by a vibrational 
identification consisting in the number of the vibrational state and in a 
percentage representing the projection of the "true" vibrational wave function 
on the specified normal mode basis.

.. code-block:: text

   Hamiltonian Parameters in C3v Formalism
   
     289 Data ; Jmax  89 ; St Dev    1.516
    4.  4.  4.          .5      Spin Statistics , Spin Y
   P0 D6           0 0 0 0 0 0 P0 D6          0 0 0 0 0 0 D0          dip
     2916.5  1533.3  3019.5  1310.8  1230.0  2342.0     1.758969E-01 9.7864000E-02     1.49885900000E-01 -1.6842752
     23    0.d+00   0     Para Number  ;  Model Accuracy Parameters
   12CH3D
   Jeu 23 jan 2020 10:11:04 CET   Hmn  Frdm         Value/cm-1  St.Dev./cm-1
      1  2(0,S+ ) 000000 S+  000000 S+  S+  S+  02   111  0.14988597768E+00 0.1163613E-08
      2  2(2,S+ ) 000000 S+  000000 S+  S+  S+  02     0 -0.15928387140E-01 0.0000000E+00
   ......................................................................................
     22  8(8,F  ) 000000 S+  000000 S+  S+  F   08     0  0.00000000000E+00 0.0000000E+00
     23  8(8,I  ) 000000 S+  000000 S+  S+  I   08     0  0.00000000000E+00 0.0000000E+00
   
   Transition Moment Parameters in C3v Formalism
   
       0 Data ; Jmax   0 ; St Dev    1.516
   *
   *
      0  Arbitrary Units
      4
   *
   *                                           Hmn  Frdm
      1  2(0,S+ ) 000000 S+  000000 S+  S-  S+  02     0  0.10000000000E+01 0.0000000E+00
      2  2(2,S+ ) 000000 S+  000000 S+  S-  S+  02     0  0.00000000000E+00 0.0000000E+00
      3  2(2,P  ) 000000 S+  000000 S+  P   P   02     0  0.00000000000E+00 0.0000000E+00
      4  2(2,D  ) 000000 S+  000000 S+  P   D   02     0  0.00000000000E+00 0.0000000E+00
   
     1 Upper Vibrational States
   
        #     v1        v2        v3        v4        v5            v6        Cv
        1  |[[ 0(0,S+ )* 0(0,S+ )* 0(0,S+ )* 0(0,S+ )* 0(0,S+ )]S+ * 0(0,S+ )]S+  >
   
     1 Lower Vibrational States
   
        #     v1        v2        v3        v4        v5            v6        Cv
        1  |[[ 0(0,S+ )* 0(0,S+ )* 0(0,S+ )* 0(0,S+ )* 0(0,S+ )]S+ * 0(0,S+ )]S+  >
   
   Spectroscopic Data Calculated through J =  99
   Imposed Frequency Range:            0.000000 -         10000.000000
   Vibrational Temperature:  300.00 Kelvin
   Rotational  Temperature:  300.00 Kelvin
   Intensity Lower Limit:  0.00E+00   Arbitrary Units
   Abundance:    1.0000
   Vibrational partition function:   1.0099E+00
   
     Calculated Transitions
   
        Frequency  Intensity     J" C"  n" #vib"    J  C   n  #vib      Lower Energy    Lower Population
   
         10.546519 4.38E-07  R   0 A1   1  1 100%   1 A2   1  1 100%        0.000000    0.408321E-04
         21.092978 5.60E-05  R   1 A2   1  1 100%   2 A1   1  1 100%        0.351794    0.122290E-03
         21.093008 4.20E-05  R   1 E    1  1 100%   2 E    2  1 100%        0.273761    0.122335E-03
   .................................................................................................
       1038.918622 1.06E+04  R  98 E   65  1 100%  99 E   65  1 100%     1702.037854    0.229224E-05
       1038.917422 1.06E+04  R  98 E   66  1 100%  99 E   66  1 100%     1702.270015    0.228969E-05
   
    Number of Calculated Transitions          6534
    First Transition      ->        0.351794 4.38E-07  R   0 A1 
    Strongest Transition  ->       25.636748 7.32E+04  R  72 A2 
    Last Transition       ->       34.781496 7.60E+03  R  98 E  
    Effective Jo range    ->   0 - 98
    Strongest Tr at Jmax  ->       34.747490 4.10E+04  R  99 A2 
    Intensity summations:
    0.24E+09   Arbitrary Units             with    threshold = 0.00E+00
    0.24E+09   Arbitrary Units             without threshold
