.. Spview documentation master file, created by
   sphinx-quickstart on Tue Jun  6 22:17:21 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Spview's documentation!
==================================

This is the documentation of version |release|.

SPVIEW is a cross-platform application for graphical assignment of high-resolution 
molecular spectra. It is possible to load, display and manipulate experimental 
and simulated spectra (``XY ASCII`` format) as well as handle spectra in various 
formats (including `HITRAN <https://hitran.org/docs/cross-sections-definitions/>`_ 
format). The lines can be assigned graphically using the mouse. Assignments can 
also be changed or deleted. Local simulations can be performed, for example, to 
facilitate assignment to partially resolved line groups. SPVIEW is also capable 
of producing peak lists from an experimental spectrum.

Ideally, Spview should be used with XTDS, which calculates spectra using the 
Dijon spectroscopy group techniques based on group theory and tensorial 
formalism, and produces files that can be used in Spview.

It is being developed at the Laboratoire Interdisciplinaire Carnot de Bourgogne 
in Dijon, by the `MARS <https://icb.u-bourgogne.fr/mars-2/>`_ group.

It's free `software <https://www.gnu.org/philosophy/free-sw.html>`_, you can 
change its `source code <https://gitlab.com/lock042/spview>`_ and distribute 
your changes.

.. figure:: ./_images/spview-screenshot.png
   :alt: screenshot
   :class: with-shadow
   :width: 100%


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: General

   Installation
   GUI
   File-format
   About
   
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Use case
   
   Assignment

