File Format
===========

Spview uses several types of file format. In this section, we'll try to detail 
these file types, most of which must respect a very strict format, inherited 
from the Fortran programs that created them.

Here's a table showing the existing format types and whether Spview handles them
as input, output or both.

.. rst-class:: table-responsive

+--------------------------------------------------------------------+---------+--------+
| File format                                                        | Input   | Output |
+====================================================================+=========+========+
| :ref:`2-columns <file-format/2-columns:2-columns File Format>`     | Yes     | No     |
+--------------------------------------------------------------------+---------+--------+
| :ref:`OPUS <file-format/OPUS:OPUS File Format>`                    | Yes     | No     |
+--------------------------------------------------------------------+---------+--------+
| :ref:`Assignment (\*.asg) <file-format/Asg:Assignment File Format>`| Yes     | Yes    |
+--------------------------------------------------------------------+---------+--------+
| :ref:`SPV (\*.spv) <file-format/Spv:SPV File Format>`              | Yes     | Yes    |
+--------------------------------------------------------------------+---------+--------+
| :ref:`TDS (spectr.t) <file-format/TDS:TDS File Format>`            | Yes     | No     |
+--------------------------------------------------------------------+---------+--------+
| :ref:`HITRAN <file-format/HITRAN:HITRAN File Format>`              | Yes     | No     |
+--------------------------------------------------------------------+---------+--------+





.. toctree::
   :hidden:
   
   file-format/2-columns
   file-format/Asg
   file-format/OPUS
   file-format/Spv
   file-format/TDS
   file-format/HITRAN
